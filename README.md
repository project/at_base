AT Base [![Build Status](https://travis-ci.org/atdrupal/at_base.module.svg?branch=v7.2)](https://travis-ci.org/atdrupal/at_base.module)
=======

Provide some more API for developer to work with Drupal 7.

### Install

    drush dl at_base
    drush atr at_base

### Requirements:

  If you do have drush installed on your machine. You can download these libraries
  manually, add them to `./sites/all/libraries`.

  - [spyc](https://github.com/mustangostang/spyc.git) revision [b7fd7f7a4ddd70c4d599a023fcda3e2b9a20d4b9](https://github.com/mustangostang/spyc/archive/b7fd7f7a4ddd70c4d599a023fcda3e2b9a20d4b9.zip)
  - [Pimple](https://github.com/fabpot/Pimple) Version [v1.1.0](https://github.com/fabpot/Pimple/archive/v1.1.1.zip)
  - [Expression Language](https://github.com/symfony/expression-language) [v2.4.0](https://github.com/symfony/expression-language/archive/v2.5.5.zip)

### Features

- [Autoload](https://github.com/atdrupal/at_base/wiki/7.x-2.x-autoload)
- [Key-Value storage](https://github.com/atdrupal/at_base/wiki/7.x-2.x-kv)
- [Config](https://github.com/atdrupal/at_base/wiki/7.x-2.x-config)
- [Service Container](https://github.com/atdrupal/at_base/wiki/7.x-2.x-service-container)
- [Easy Routing](https://github.com/atdrupal/at_base/wiki/7.x-2.x-easy-routing)
- [Easy Breadcrumb](https://github.com/atdrupal/at_base/wiki/7.x-2.x-easy-breadcrumb)
- [Easy Block](https://github.com/atdrupal/at_base/wiki/7.x-2.x-easy-routing)
- [Useful functions](https://github.com/atdrupal/at_base/wiki/7.x-2.x-functions)
- [Twig template](https://github.com/atdrupal/at_base/wiki/7.x-2.x-twig-recipes)
- [drush at-require](https://github.com/atdrupal/at_base/wiki/7.x-2.x-drush-at-require)
- [and more…](https://github.com/atdrupal/at_base/wiki/_pages)
